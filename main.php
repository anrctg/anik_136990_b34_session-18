<?php
/**
 * Crated by anik
 */




class BITM{
    //property
    public $windows;
    public $door;
    public $chair;
    public $table;
    public $whiteboard;



    //method 1
    public function coolTheAir(){
        echo "I am cooling the air";
    }
    //method 2
    public function compute(){
        echo "I am computing calculation";
    }

    //method 3
    public function show(){
        echo $this->windows."<br />";
        echo $this->door."<br />";
        echo $this->chair."<br />";
        echo $this->table."<br />";
        echo $this->whiteboard."<br />";
    }



    //method 4
    public function setVal(){
        $this->windows = "Windows value is set";
        $this->door = "door value is set";
        $this->chair = "chair value is set";
        $this->table = "table value is set";
        $this->whiteboard = "whiteboard value is set";
    }
    //method 5
    public function

}


$obj = new BITM();

$obj->setVal();
//$obj->compute();

$obj->show();
